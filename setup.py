from setuptools import setup, find_packages

setup(name='database',
      version='0.1',
      description='',
      url='http://github.com/zznixt07',
      author='zznixt07',
      author_email='zznixt07@protonmail.com',
      license='MIT',
      packages=['database'],
      zip_safe=False
)